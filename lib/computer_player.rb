
class ComputerPlayer

  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = mark
  end

  def get_move
    @board.empty_positions.each do |pos|
      return pos if @board.would_win?(pos, @mark)
    end
    @board.empty_positions.sample
  end

  def display(board)
    @board = board
  end

  # def to_s
  #   @mark.to_s
  # end
end
