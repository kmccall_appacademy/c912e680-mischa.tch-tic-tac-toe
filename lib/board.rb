

class Board
  attr_reader :grid, :victory

  def initialize(grid = nil)
    @grid = grid || Array.new(3) {Array.new(3)}
    @victory = nil
  end

  def place_mark(pos, mark)
    x, y = pos
    @grid[x][y] = mark
  end

  def empty?(pos)
    x, y = pos
    @grid[x][y] == nil
  end

  def winner
    lines.each do |line|
      if line.all? {|el| el == :X}
        @victory = :X
      elsif line.all? {|el| el == :O}
        @victory = :O
      end
    end
    victory
  end

  def lines
    h_lines = grid
    v_lines = grid.transpose
    diag1 = [grid[0][0], grid[1][1], grid[2][2]]
    diag2 = [grid[2][0], grid[1][1], grid[0][2]]
    lines = h_lines + v_lines + [diag1] + [diag2]
  end

  def over?
    if winner != nil
      return true
    elsif @victory == nil && !board_in_progress
      return true
    end
    false
  end

  def board_empty?
    empty = true
    lines.each do |line|
      if line.all? {|el| el != nil}
        empty = false
      end
    end
    empty
  end

  def board_in_progress
    lines.flatten.any? {|el| el == nil}
  end

  def to_s
    @grid.map do |row|
        row.map {|el| el.nil? ? "_" : el }.join(" ")
    end.join("\n")
  end

  def empty_positions
    positions.select {|pos| self.empty?(pos)}
  end

  def would_win?(pos, mark)
    place_mark(pos, mark)
    would_win = winner == mark
    self[pos] = nil
    would_win
  end

  def valid?(most_common_vowel)
    move.all? {|coord| coord.between?(0, 2)}
    empty_positions.include?(move)
  end

  def positions
    pos = []
    3.times do |row|
      3.times {|col| pos << [row, col]}
    end
    pos
  end

  def won?(mark)
    lines.any? do |line|
      line.all? {|pos| self[pos] == mark}
    end
  end


  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos, val)
    x, y = pos
    @grid[x][y] = val
  end

end



if __FILE__ == $PROGRAM_NAME

  p1 = HumanPlayer.new('You')
  p2 = HumanPlayer.new('Tim')
  g = Game.new(p1, p2)
  g.play

end
