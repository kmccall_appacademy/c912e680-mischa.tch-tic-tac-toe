require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_reader :board, :current_player

  def initialize(pl1, pl2)
    @board = Board.new
    @player_one = pl1
    @player_two = pl2
    @player_one.mark = :X
    @player_two.mark = :O
    @current_player = @player_one
  end

  def play_turn
    @current_player.display(@board)
    move = current_player.get_move
    @board.place_mark(move, current_player.mark)
    switch_players!
  end


  def switch_players!
    if @current_player == @player_one
      @current_player = @player_two
    else
      @current_player = @player_one
    end
  end

  def play
    until @board.over?
      play_turn
    end
    conclude
  end

  def conclude
    puts
    puts @board.to_s
    puts
    puts "Congratulations, #{@board.winner}, you win"
    puts
  end

end


if __FILE__ == $PROGRAM_NAME

  p1 = HumanPlayer.new('You')
  p2 = ComputerPlayer.new('Tim')
  g = Game.new(p1, p2)
  g.play

end
